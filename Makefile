.PHONY: proto
proto:
	protoc --go_out=. --go-grpc_out=. stabilisense.proto
	cp gitlab.com/simon.schlumpf/stabilisense-proto/* .
